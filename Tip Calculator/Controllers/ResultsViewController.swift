//
//  ResultsViewController.swift
//  Tip Calculator
//
//  Created by Marcus Mimnagh on 8/7/20.
//  Copyright © 2020 Marcus Mimnagh. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {

    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var settingsLabel: UILabel!
    
    var grandTotal: String!
    var amountOfPeople: String!
    var tipAmount: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        totalLabel.text = "$ \(grandTotal!)"
        settingsLabel.text = "Split between a party of \(amountOfPeople!), with \(tipAmount)% tip. "
    }
    
    @IBAction func recalculatePressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}
