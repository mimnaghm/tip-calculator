//
//  CalculatorViewController.swift
//  Tip Calculator
//
//  Created by Marcus Mimnagh on 8/7/20.
//  Copyright © 2020 Marcus Mimnagh. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet var billTextField: UITextField!
    @IBOutlet var peopleCount: UILabel!
    
    @IBOutlet var zeroPctButton: UIButton!
    @IBOutlet var tenPctButton: UIButton!
    @IBOutlet var twentyPctButton: UIButton!
    
    var percentageSelected: Double = 0.0
    var totalAmountPerPerson: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func tipChanged(_ sender: UIButton) {
        clearSelectedState()
        sender.isSelected = true
        
        percentageSelected = Double(sender.tag) / Double(100)
        
        billTextField.resignFirstResponder()
    }
    
    func clearSelectedState() {
        [zeroPctButton, tenPctButton, twentyPctButton].forEach {
            $0?.isSelected = false
        }
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        peopleCount.text = Int(sender.value).description
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {

            let billAmount = Double(billTextField.text!)
            let splitAmount = Double(peopleCount.text!)
            
            let tipValue = billAmount! * percentageSelected
            let grandTotal = billAmount! + tipValue
            let amountPerPerson = grandTotal / splitAmount!
            
            totalAmountPerPerson = amountPerPerson
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! ResultsViewController
        let oA = String(format: "%.2f", totalAmountPerPerson)
        let percentTip = String(format: "%.2f", percentageSelected)
        
        vc.grandTotal = oA
        vc.amountOfPeople = peopleCount.text
        vc.tipAmount = percentTip
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
